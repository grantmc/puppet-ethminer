class ethminer::install inherits ethminer {

  $source = "https://github.com/ethereum-mining/ethminer/releases/download/v${ethminer::version}/ethminer-${ethminer::version}-Linux.tar.gz";

  $filename = basename($source);

  archive { "${ethminer::install_dir}/${filename}":
    ensure        => present,
    extract       => true,
    extract_path  => $ethminer::install_dir,
    source        => $source
  }
}