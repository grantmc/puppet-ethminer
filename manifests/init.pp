# Class: ethminer
# ===========================
#
# Full description of class ethminer here.
#
# Parameters
# ----------
#
# Authors
# -------
#
# Author Name <sander.bilo@gmail.com>
#
# Copyright
# ---------
#
# Copyright 2017 Sander Bilo, unless otherwise noted.
#
class ethminer(
  String $version                   = '0.12.0',
  Array[String] $env_vars           = [
    'GPU_FORCE_64BIT_PTR=0',
    'GPU_MAX_HEAP_SIZE=100',
    'GPU_USE_SYNC_OBJECTS=1',
    'GPU_MAX_ALLOC_PERCENT=100',
    'GPU_SINGLE_ALLOC_PERCENT=100'],
  Boolean $enabled                  = true,
  Stdlib::AbsolutePath $install_dir = '/opt/ethminer',
  Optional[String] $email           = 'sander.bilo@gmail.com',
  Optional[Array[String]] $pools    = ['eth-eu1.nanopool.org:9999', 'eth-eu2.nanopool.org:9999'],
  Optional[String] $wallet          = '0x005463668233189e1a954F5BCbA37605C54b7DFD',
) {

  contain ethminer::install
  contain ethminer::config
  contain ethminer::service

  Class['::ethminer::install']
  -> Class['::ethminer::config']
  ~> Class['::ethminer::service']
}
